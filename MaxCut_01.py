
from libraries import *

import networkx as nx
import matplotlib.pyplot as plt 
from sympy import lambdify

#=======================================================================================
# Defining a MaxCut problem 
#=======================================================================================

class MaxCut():
	def __init__(self, edgeList=[]):
		# node number starts from 0
		self.nodeNum 		= 1
		self.edgeNum		= len(edgeList)
		self.edgeList		= [] 


		for edge in edgeList:
			self.nodeNum 	= self.nodeNum if self.nodeNum >= max(edge[:2])+1  else int(max(edge[:2]) + 1)

			if len(edge) == 2:
				self.edgeList.append((edge[0], edge[1], 1.0))  
			else:
				self.edgeList.append(edge)


		self.nodeList		= np.arange(0, self.nodeNum, 1)

		self.Graph 			= nx.Graph()
		self.Graph.add_nodes_from(self.nodeList)
		self.Graph.add_weighted_edges_from(self.edgeList)

		self.size 			= 2**self.nodeNum

		self.GenerateMatrices()


	#==================================================================================
	# draw graph
	def drawGraph(self):
		colors       = ['lightblue' for node in self.Graph.nodes()]
		default_axes = plt.axes(frameon=True)
		pos          = nx.spring_layout(self.Graph)

		nx.draw_networkx(self.Graph, node_color=colors, node_size=600, alpha=1, ax=default_axes, pos=pos)
		plt.savefig("figures/graph.png", dpi=300)


	#==================================================================================
	# Pauli gates
	def GenerateMatrices(self):
		x_gate 	= np.array([[0.,1.], [1.,0.]])
		y_gate 	= np.array([[0., complex(0, -1)], [complex(0.,1.), 0.]])
		z_gate 	= np.array([[1.,0.], [0,-1.]])

		i_gate = np.eye(self.size)
		x_nodes = [1 for _ in self.nodeList]
		y_nodes = [1 for _ in self.nodeList]
		z_nodes = [1 for _ in self.nodeList]

		tempI 	= 1
		for node in self.nodeList:
			x_nodes[node] 	= np.kron(tempI, x_gate)
			y_nodes[node] 	= np.kron(tempI, y_gate)
			z_nodes[node] 	= np.kron(tempI, z_gate)
			tempI			= np.kron(tempI, np.eye(2))

			for _ in range(node + 1, self.nodeNum):
				x_nodes[node] = np.kron(x_nodes[node], np.eye(2))
				y_nodes[node] = np.kron(y_nodes[node], np.eye(2))
				z_nodes[node] = np.kron(z_nodes[node], np.eye(2))

		
		self.i_matrix 		= Matrix(i_gate)
		self.x_nodes_matrix = [Matrix(x_nodes[node]) for node in self.nodeList]
		self.y_nodes_matrix = [Matrix(y_nodes[node]) for node in self.nodeList]
		self.z_nodes_matrix = [Matrix(z_nodes[node]) for node in self.nodeList]
		
		z_edges 			= {edge: np.dot(z_nodes[edge[0]], z_nodes[edge[1]]) for edge in self.edgeList }
		self.z_edges_matrix = {edge: Matrix(z_edges[edge]) for edge in self.edgeList}

		hamiltonian 		= np.zeros(self.size)
		for edge in self.edgeList: 
			hamiltonian =  hamiltonian + z_edges[edge]

		
		self.circuit_operator = 0.5 * ((self.edgeNum * i_gate ) - hamiltonian)
		self.hamiltonian 	= Matrix(hamiltonian/float(self.size))



	#==================================================================================
	def pauliGates(self, gate_type, index=0):
		
		if gate_type == 'i':
			return deepcopy(self.i_matrix)

		elif gate_type == 'x':
			return deepcopy(self.x_nodes_matrix[index])

		elif gate_type == 'y':
			return deepcopy(self.y_nodes_matrix[index])

		elif gate_type == 'z':
			return deepcopy(self.z_nodes_matrix[index])

		elif gate_type == 'zz':
			return deepcopy(self.z_edges_matrix[index])

		elif gate_type == 'hamiltonian':
			return deepcopy(self.hamiltonian)

	#==================================================================================
	def findPattern(self):
		self.patterns 			= {}

		for edge in self.edgeList:
			node1 = edge[0]
			node2 = edge[1]

			neighbor1 			= set(self.Graph.neighbors(node1))
			neighbor2 			= set(self.Graph.neighbors(node2))

			common_neighbors 	= neighbor2.intersection(neighbor1)

			neighbor_num1 		= len(neighbor1 ) - 1
			neighbor_num2 		= len(neighbor2 ) - 1

			key_min				= min(neighbor_num1, neighbor_num2)
			key_max				= max(neighbor_num1, neighbor_num2)
			key_com				= len(common_neighbors)

			if (key_min, key_max, key_com) in self.patterns.keys():
				self.patterns[(key_min, key_max, key_com)] += 1
			else:
				self.patterns[(key_min, key_max, key_com)] = 1

		



		 




