from libraries import *
import_module("QAOAExpectedValue")
from QAOAExpectedValue import *


edgeList 		= [(1,0), (1,2), (0,3), (0,4), (2,4), (3,2)]
# edgeList 		= [(0,1),(0,2),(1,2),(3,2),(3,4),(4,2)] 
# edgeList 		= [(0, 1), (0, 3), (1, 2), (2, 3)]
# edgeList 		= [(0,1), (0,2), (1,2)] 
# edgeList 		= [(0,1)] 
# edgeList 		= [(0,1), (1,2)] 

# paramValueDic 	= {'b_0': -0.78360917, 'g_0': 1.56790523, 'b_1': -0.7854304, 'g_1': -1.17700398}
paramValueDic 	= {'b_0': -1.17700398, 'g_0': 1.56790523, 'b_1': -0.7854304, 'g_1': -0.78360917}


problem			= MaxCut(edgeList)
QAOA 			= MaxCutQAOA(problem, 1)

problem.drawGraph()

QAOA.miloSolver(60)

QAOA.quantumExpectedValue(paramValueDic)
QAOA.quantumOptimizer(n_layers=1, simulation=True)

# print_header("Direct calculation", 50, '=')
# method2 		= QAOA.expectedValueFormula(2)
# QAOA.evaluate(method2, paramValueDic)


# print_header("Original formula for p=1", 50, '=')
# method3 		= QAOA.expectedValueFormula(3)
# QAOA.evaluate(method3, paramValueDic)


print_header("THE END OF FILE", 50, '=')