# useful additional packages 
from __future__ import print_function

#import math tools
# import numpy as np
from numpy import sin, cos
from numpy import complex

from sympy import *
from sympy.interactive.printing import init_printing
from sympy.matrices import Matrix, eye, zeros, ones, diag, GramSchmidt
from sympy import sin, cos, Matrix, simplify, trigsimp, factor

from copy import deepcopy, copy
from collections import namedtuple

from pennylane import numpy as np

import os
import sys
import re

import importlib.util


def latest_version(lib_name):
	all_ver = list(filter(lambda x: re.split('_|\.', x)[0] == lib_name , os.listdir(os.getcwd())))
	max_ver = max(all_ver, key = lambda x: int(re.split('_|\.', x)[-2]))
	return max_ver.split(".")[0]

	
def import_module(lib_name):
	module_lib = latest_version(lib_name)
	path = os.getcwd() +'/' + module_lib + '.py'

	spec = importlib.util.spec_from_file_location(module_lib, path)
	module = importlib.util.module_from_spec(spec)
	spec.loader.exec_module(module)
	sys.modules[lib_name] = module


def print_header(*argv):
		# argv[-2]: number of separator char    argv[-1]: separator char
		string = ''
		for s in range(argv[-2]): 
			string += argv[-1]

		print()
		print(string)
		for arg in argv[:-2]:
			print(arg,sep="",end="")
		print("\n", string, sep="")


trigtuple = namedtuple('trig_name',['trig_type', 'angle_coef', 'angle_param'])



