
from libraries import *

import_module("MaxCut")
from MaxCut import *

#=======================================================================================
# importing 
import matplotlib.pyplot as plt 
from   matplotlib import cm
from   matplotlib.ticker import LinearLocator, FormatStrFormatter

#=======================================================================================
# importing Qiskit
from qiskit import Aer, IBMQ
from qiskit import QuantumRegister, ClassicalRegister, QuantumCircuit, execute
from qiskit.extensions import *
from qiskit.quantum_info import Operator

#=======================================================================================
from qiskit.providers.ibmq import least_busy
from qiskit.tools.monitor import job_monitor
from qiskit.visualization import plot_histogram

#=======================================================================================
import pennylane as qml
from pennylane import numpy as np

#=======================================================================================
from gurobipy import *


#=======================================================================================
# QAOA Expected Value
#=======================================================================================
class MaxCutQAOA(MaxCut, object):
	def __init__(self, maxCut, p=1):
		self.p 		= p
		self.maxCut = maxCut

	#==================================================================================
	def mixingOperator(self, method, level, left=True): 
		# method 	-> 	1: (2MP)C		2: MPCPM
		# level  	->	an int number less than p
		# left		->	True-False variable for the inverse of operator

		if isinstance(level, int):
			angle 		= 'b_' + str(level)
			angle 		= Symbol(angle)
		else: NotImplementedError

		if method == 1:
			cosTerm 	= cos(2*angle)
			sinTuple 	= sin(2*angle)

		elif method == 2:
			cosTerm 	= cos(angle)
			sinTuple 	= sin(angle)

		else: NotImplementedError

		sign 				= 1 if left else -1
		signedImagSinTerm 	= complex(0,sign ) * sinTuple

		cosTerm_iMatrix 	= cosTerm * self.maxCut.pauliGates('i')
		mixing_operator 	= self.maxCut.pauliGates('i')

		for node in self.maxCut.nodeList:
			# temp_matrix 	= deepcopy(cosTerm_iMatrix)
			temp_matrix		= cosTerm * self.maxCut.pauliGates('i') + signedImagSinTerm * self.maxCut.pauliGates('x', node)
			mixing_operator	= mixing_operator * temp_matrix

		return mixing_operator


	#==================================================================================	
	def phaseSeparationOperator(self, method, level, left=True, phaseShift=False):
		# method 		-> 	1: (2MP)C		2: MPCPM
		# level  		->	an int number less than p
		# left			->	True-False variable for the inverse of operator
		# phaseShift 	->	True-False varibale for considering term e^(0.5 * i * edgeNum * gamma)

		if isinstance(level, int):
			angle 		= 'g_' + str(level)
			angle 		= Symbol(angle)
		else: NotImplementedError

		if method == 1:
			cosTerm 	= cos(angle)
			sinTuple 	= sin(angle)

		elif method == 2:
			cosTerm 	= cos(0.5*angle)
			sinTuple 	= sin(0.5*angle)

		else: NotImplementedError

		sign 						= -1 if left else 1
		signedImagSinTerm 			= complex(0,sign )*sinTuple

		cosTerm_iMatrix 			= cosTerm * self.maxCut.pauliGates('i')	
		phase_separation_operator 	= self.maxCut.pauliGates('i')


		for edge in self.maxCut.edgeList:
			temp_matrix 				= deepcopy(cosTerm_iMatrix)
			temp_matrix					= temp_matrix + signedImagSinTerm * self.maxCut.pauliGates('zz', edge)
			phase_separation_operator	= phase_separation_operator * temp_matrix


		if phaseShift: 
			cosTerm 					= cos(0.5 * self.maxCut.edgeNum * angle)
			sinTerm 					= complex(0, -sign) * sin(0.5 * self.maxCut.edgeNum * angle)
			shift 						= cosTerm + sinTerm

			phase_separation_operator 	= shift * phase_separation_operator



		return phase_separation_operator


	#==================================================================================
	def ObjectiveValueFromSolution(self, solution):
		cost 		= 0.0
		solution 	= [int(num) for num in solution]

		if len(solution) != self.maxCut.nodeNum:
			return NotImplementedError
		
		for edge in self.maxCut.Graph.edges():
			weight		= self.maxCut.Graph[edge[0]][edge[1]]['weight']
			first_node	= solution[edge[0]]
			second_node	= solution[edge[1]]
			cost 		+= weight * (first_node*(1 - second_node) + second_node*(1 - first_node))

		return int(cost)


	#==================================================================================
	def quantumExpectedValue(self, paramValueDic, simulation=True):
		if simulation:
			QAOA = QuantumCircuit(self.maxCut.nodeNum, self.maxCut.nodeNum)

			QAOA.h(self.maxCut.nodeList)
			# QAOA.barrier()

			for level in range(self.p):
				gamma 	= paramValueDic['g_' + str(level)]
				beta 	= paramValueDic['b_' + str(level)]

				phase_sep_operator = HamiltonianGate(self.maxCut.circuit_operator, gamma)
				QAOA.append(phase_sep_operator, range(self.maxCut.nodeNum))

				QAOA.barrier()
				QAOA.rx(2*beta, self.maxCut.nodeList)
				QAOA.barrier()

			# QAOA.barrier()
			QAOA.measure(self.maxCut.nodeList, self.maxCut.nodeList)

			QAOA.draw(output='text', filename='figures/QAOA_circuit.txt')

			

			backend 		= Aer.get_backend("qasm_simulator")
			shots			= 10_000

			simulate 		= execute(QAOA, backend=backend, shots=shots)
			QAOA_results	= simulate.result()

			counts 			= QAOA_results.get_counts()

			plot_histogram(counts, figsize = (8,8), bar_labels = False).savefig('figures/solution_probabilities.png', dpi=300)

			average_objVal	= 0.0
			best_objVal		= 0.0
			best_solution	= 0.0
			histogram 		= {edgeInd: 0 for edgeInd in range(self.maxCut.edgeNum + 1)}


			for (solution, count) in counts.items():
				objective_value = self.ObjectiveValueFromSolution(solution)
				average_objVal	= average_objVal + count * objective_value

				histogram[objective_value]	= histogram.get(objective_value, 0) + count

				if best_objVal < objective_value:
					best_objVal 	= objective_value
					best_solution	= solution

			average_objVal /= float(shots)

			# setA = set(node for node in self.maxCut.nodeList if int(best_solution[node]) == 1) 
			# setB = set(node for node in self.maxCut.nodeList if int(best_solution[node]) == 0) 

			print_header("Summery of results the QAOA algorithm", 50, '=')

			print('The sampled mean value:\t\t\t\t', average_objVal)

			print('The probability of finding the best solution:\t', np.round(histogram[best_objVal]/shots, 2))
			print("The best objective value:\t\t\t", best_objVal)
			# print("Set A:\t\t", setA)
			# print("Set B:\t\t", setB)
			
	
			plot_histogram(histogram, figsize = (8,6), bar_labels = False).savefig('figures/objective_probabilities.png', dpi=300)



#==================================================================================
	def quantumOptimizer(self, n_layers, simulation=True):
		if simulation:
			n_wires 	= self.maxCut.nodeNum

			dev 		= qml.device("default.qubit", wires=n_wires, analytic=True, shots=1)

			def U_B(beta):
				for wire in range(n_wires): qml.RX(2 * beta, wires=wire)

			phase_sep_operator 			= HamiltonianGate(self.maxCut.circuit_operator, 1)
			phase_sep_operator_matrix 	= np.array(phase_sep_operator.to_matrix())


			def U_C(gamma):
				temp = phase_sep_operator_matrix**gamma
				qml.QubitUnitary(temp, wires=range(n_wires) )

				
			def comp_basis_measurement(wires):
				return qml.Hermitian(np.diag(range(self.maxCut.size)), wires=range(n_wires))

			measurement_operator = np.array(self.maxCut.circuit_operator)

			@qml.qnode(dev)
			def circuit(gammas, betas, measurement_phase=True, n_layers=1):

				# apply Hadamards to get the n qubit |+> state
				for wire in range(n_wires):
					qml.Hadamard(wires=wire)

				# p instances of unitary operators
				for i in range(n_layers):
					U_C(gammas[i])
					U_B(betas[i])

				if measurement_phase:
					# measurement phase
					return qml.sample(comp_basis_measurement(range(n_wires)))

				# during the optimization phase we are evaluating a term
				# in the objective using expval
				return qml.expval(qml.Hermitian(measurement_operator, wires=range(n_wires)))


			print("\np={:d}".format(n_layers))


			# minimize the negative of the objective function
			def objective(params):
				gammas 		= params[0]
				betas 		= params[1]
				return - circuit(gammas, betas, measurement_phase=False, n_layers=n_layers)


			# initialize optimizer: Adagrad works well empirically
			opt 			= qml.AdagradOptimizer(stepsize=0.5)


			# initialize the parameters near zero
			init_params 	= 0.01 * np.random.rand(2, n_layers)

			# optimize parameters in objective
			params 			= init_params
			steps 			= 30

			for i in range(steps):
				params = opt.step(objective, params)
				if (i + 1) % 5 == 0:
					print("Objective after step {:5d}: {: .7f}".format(i + 1, -objective(params)))

			# sample measured bitstrings 100 times
			bit_strings 	= []
			n_samples 		= 100

			for i in range(0, n_samples):
				bit_strings.append(int(circuit(params[0], params[1], measurement_phase=True, n_layers=n_layers)))

			# print optimal parameters and most frequently sampled bitstring
			counts 					= np.bincount(np.array(bit_strings))
			most_freq_bit_string 	= np.argmax(counts)

			print("Optimized (gamma, beta) vectors:\n{}".format(params))
			print("Most frequently sampled bit string is: {:04b}".format(most_freq_bit_string))
			

	#==================================================================================
	# method -> 	1: (2MP)C		2: MPCPM		3: updated theorem		4: theorem
	def expectedValueFormula(self, method):

		if method == 1:
			result = self.maxCut.pauliGates('hamiltonian')

			for level in range(self.p):

				phase_separation_operator	= self.phaseSeparationOperator(method, level, left=True, phaseShift=False)
				mixing_operator 			= self.mixingOperator(method, level, left=True)
				

				result 						= mixing_operator * result
				result 						= phase_separation_operator * result
				

			expected_value 			= (self.maxCut.edgeNum -  sum(result) )*0.5

			return expected_value

		elif method == 2:
			result = self.maxCut.pauliGates('hamiltonian')

			for level in range(self.p - 1, -1, -1):

				left_phase_separation_operator	= self.phaseSeparationOperator(method, level, left=True, phaseShift=True)
				left_mixing_operator 			= self.mixingOperator(method, level, left=True)


				right_mixing_operator 			= self.mixingOperator(method, level, left=False)
				right_phase_separation_operator	= self.phaseSeparationOperator(method, level, left=False, phaseShift=True)
				

				result 							= left_mixing_operator * result * right_mixing_operator 
				result 							= simplify(result) if level == self.p - 1 else result 
				result 							= left_phase_separation_operator * result * right_phase_separation_operator

			expected_value 				= (self.maxCut.edgeNum -  sum(result))*0.5
			# expected_value				= simplify(expected_value)

			return expected_value



		elif method == 3 and self.p == 1:
			b_0 				= Symbol('b_0')
			g_0 				= Symbol('g_0')

			self.maxCut.findPattern()
			expected_value 		= 0

			for (key, value) in self.maxCut.patterns.items():
				first 			= 0.5 + 0.25 * (sin(4*b_0) * sin(g_0) * (cos(g_0)**key[0] + cos(g_0)**key[1]) )
				second 			= 0.25 * sin(2*b_0)**2 * cos(g_0)**(key[0] + key[1] - 2* key[2]) * (cos(2*g_0)**key[2] - 1) 
				expected_value += value * (first + second)			

			return expected_value

		else:
			NotImplementedError

	#==================================================================================
	def evaluate(self, expected_value, paramValueDic):
		if expected_value == None:
			print("p is not equal to 1!")
		
		else:
			param 	= {Symbol(key): value for (key, value) in paramValueDic.items()}
			result 	= complex(expected_value.evalf(subs= param) )

			print('The expected value of the circuit:\n', expected_value, '\n', sep='')
			print('The expected value for the given parameters:\t{:.3f} + {:.3f}i'.format(result.real, result.imag))
		 

	#==================================================================================
	def printMatrix(self, matrix):
		rowN = matrix.shape[0]
		colN = matrix.shape[1]

		for row in range(rowN):
			for col in range(colN):

				if matrix[row,col] != 0:
					print("row:", str(row), "col:", str(col), "->", matrix[row,col], '\n')


	#==================================================================================
	def miloSolver(self, time_limit):
		model 					= Model("MAX-CUT")
		model.Params.timeLimit 	= time_limit
		model.Params.OutputFlag = 0

		x = [model.addVar(vtype=GRB.BINARY, name="x(%i)" %node) for node in self.maxCut.nodeList]
		z = {edge: model.addVar(vtype=GRB.BINARY, name="z(%i,%i)" %(edge[0], edge[1])) \
		for edge in self.maxCut.edgeList}

		model.setObjective(quicksum(z[edge] for edge in self.maxCut.edgeList), GRB.MAXIMIZE)

		for edge in self.maxCut.edgeList:
			model.addConstr(z[edge] <= x[edge[0]] + x[edge[1]])
			model.addConstr(z[edge] <= 2 - x[edge[0]] - x[edge[1]])

		model.optimize()

		self.best_objective_milo 	= model.objVal
		self.setA 					= set(node for node in self.maxCut.nodeList if int(x[node].x) == 1)
		self.setB 					= set(node for node in self.maxCut.nodeList if int(x[node].x) == 0)

		print_header("Summery of results MILO solver", 50, '=')

		if model.status == GRB.Status.OPTIMAL: print("The optimal solution is found.")
		else: print('Optimization ended with status %d' % model.status)

		print("Objective value:", self.best_objective_milo)
		print("Set A:\t\t", self.setA)
		print("Set B:\t\t", self.setB)



		





	


